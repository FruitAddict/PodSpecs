Pod::Spec.new do |s|
s.name             = "KSKML"
s.version          = "1.0"
s.summary          = "Moduł KtoMaLek do VisiMed i KML."
s.homepage         = "kamsoft.pl"
s.author           = { "Mateusz Popiało" => "mpopialo@kamsoft.pl" }
s.source           = { :git => "https://gitlab.com/FruitAddict/KMLTest.git", :tag => s.version }

s.platform     = :ios, '9.0'
s.requires_arc = true
s.source_files = 'KSKML/*.swift'
s.framework = 'UIKit'
s.dependency 'Alamofire', '~> 4.2.0'
s.module_name = 'KSKML'
end
